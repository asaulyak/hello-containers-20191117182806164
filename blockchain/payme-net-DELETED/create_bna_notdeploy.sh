#!/bin/bash
networkName=payme-net
# cardName=payme-network
echo 'Please provide '$networkName' network version (e.g. 0.0.1)'
read versionInput
# add check for letters
#if [[ -n ${versionInput//[0-9]/} ]]; then
#    echo "Contains letters!"
#fi
echo Creating $networkName version = $versionInput
composer archive create -t dir -n .

echo 'Copying .bna file'
# cp payme-net@0.0.4.bna payme-net.bna
cp $networkName@$versionInput.bna $networkName.bna

# echo Installing network
# composer network install -c adminCard -a payme-net.bna
# composer network install -c adminCard -a $networkName.bna

# Upgrade network after instalation. Only needed if not for the first time
# composer network upgrade -n payme-net -V 0.0.3 -c adminCard
# composer network upgrade -n $networkName -V $versionInput -c adminCard

# echo Starting $networkName with version = $versionInput
# not needed after upgrade
# composer network start -c adminCard -n payme-net -V 0.0.2 -A admin -C ./credentials/admin-pub.pem -f delete_me.card
# composer network start -c adminCard -n $networkName -V $versionInput -A admin -C ./credentials/admin-pub.pem -f delete_me.card