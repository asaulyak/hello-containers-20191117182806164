#!/bin/bash
# from http://dailyraisin.com/read-json-value-in-bash/
function readJson {  
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi; 

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    echo "Error: Cannot find \"${2}\" in ${1}" >&2;
    exit 1;
  else
    echo $VALUE ;
  fi; 
}

networkName=`readJson package.json name` || exit 1;
networkVersion=`readJson package.json version` || exit 1;
adminCardName=adminCardBCNet
echo 'Creating new bna for '$networkName' version '$networkVersion
read -p "Continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes. Proceeding with bna creation";;
  n|N ) echo "no. Exiting script"; exit;;
  * ) echo "invalid input... exiting script"; exit;;
esac

echo 'Creating '$networkName' version = '$networkVersion
composer archive create -t dir -n .

echo 'Copying .bna file'
cp $networkName@$networkVersion.bna $networkName.bna
echo 'Moving release .bna'
mv $networkName@$networkVersion.bna dist/$networkName@$networkVersion.bna