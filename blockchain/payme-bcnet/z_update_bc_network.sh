#!/bin/bash
# from http://dailyraisin.com/read-json-value-in-bash/
function readJson {  
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi; 

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    echo "Error: Cannot find \"${2}\" in ${1}" >&2;
    exit 1;
  else
    echo $VALUE ;
  fi; 
}

networkName=`readJson package.json name` || exit 1;
networkVersion=`readJson package.json version` || exit 1;
adminCardName=adminCardBCNet
echo 'Installing network '$networkName' version '$networkVersion' with admin credentials: '$adminCardName
read -p "Continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes. Proceeding with installation";;
  n|N ) echo "no. Exiting script"; exit;;
  * ) echo "invalid input... exiting script"; exit;;
esac
# composer network install -c adminCardBCNet -a payme-bcnet.bna
composer network install -c $adminCardName -a $networkName.bna

echo 'Upgrading network '$networkName' version '$networkVersion' with admin credentials: '$adminCardName
read -p "Continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes. Proceeding upgrading network";;
  n|N ) echo "no. Exiting script"; exit;;
  * ) echo "invalid input... exiting script"; exit;;
esac
# Upgrade network after instalation. Only needed if installed not for the first time
# composer network upgrade -n payme-bcnet -V 0.0.13 -c adminCardBCNet
composer network upgrade -n $networkName -V $networkVersion -c $adminCardName