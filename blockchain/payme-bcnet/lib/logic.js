/**
 * Transfer money
 * @param {org.bcnet.payme.pay} pay - the pay transaction
 * @transaction
 */
function pay(payRequest) {
    console.log('pay');
    if (!payRequest.payerEmail.trim()) {
        throw new Error('Value for payerEmail was expected');
    }
    if (!payRequest.amount || payRequest.amount <= 0) {
        throw new Error('Value for payerEmail greater than 0 was expected');
    }
    if (!payRequest.forAccountEmail.trim()) {
        throw new Error('Value for forAccountEmail was expected');
    }
    if (!payRequest.toAccountEmail.trim()) {
        throw new Error('Value for toAccountEmail was expected');
    }
    if (payRequest.payerEmail.trim() == payRequest.toAccountEmail.trim()) {
        throw new Error('Payer can not transfer money to himself');
    }
    var namespace = 'org.bcnet.payme';

    // get account registry
    return getParticipantRegistry(namespace + '.Account')
        .then(function (accountRegistry) {
            // find payer account
            return accountRegistry.exists(payRequest.payerEmail)
                .then(function (exists) {
                    if (!exists) {
                        throw new Error('Payer account does not exist:' + exists);
                    } else {

                    }
                })
                .then(function () {
                    return accountRegistry.exists(payRequest.forAccountEmail)
                        .then(function (exists) {
                            if (!exists) {
                                throw new Error('For account does not exist:' + exists);
                            } else {

                            }
                        });
                })
                .then(function () {
                    return accountRegistry.exists(payRequest.toAccountEmail)
                        .then(function (exists) {
                            if (!exists) {
                                throw new Error('To account does not exist:' + exists);
                            } else {

                            }
                        });
                })
                .then(function () {
                    return accountRegistry.get(payRequest.payerEmail)
                        .then(function (payerAccount) {
                            // update payer account
                            if (payerAccount.balance < payRequest.amount) {
                                throw new Error('Payer does not have enough funds');
                            } else {
                                payerAccount.balance -= payRequest.amount;
                                return accountRegistry.update(payerAccount);
                            }
                        })
                        // do not update toAccount until transfer confirmed
                        // .then(function () {
                        //     // update to account
                        //     return accountRegistry.get(payRequest.toAccountEmail)
                        //         .then(function (toAccount) {
                        //             toAccount.balance += payRequest.amount;
                        //             return accountRegistry.update(toAccount);
                        //         });
                        // })
                        .then(function () {
                            // create new transfer
                            return getAssetRegistry(namespace + '.Transfer')
                                .then(function (assetRegistry) {
                                    // create new account and add it
                                    var transfer = getFactory().newResource(namespace, 'Transfer', guid());
                                    transfer.payerEmail = payRequest.payerEmail;
                                    transfer.amount = payRequest.amount;
                                    transfer.forAccountEmail = payRequest.forAccountEmail;
                                    transfer.toAccountEmail = payRequest.toAccountEmail;
                                    transfer.confirmationCode = s4() + s4();
                                    transfer.createdTS = getCurrentTimeStamp();
                                    return assetRegistry.add(transfer);
                                })
                        });
                });
        });
}

/**
 * Cancel transfer
 * @param {org.bcnet.payme.cancelTransfer} cancelTransfer - the cancel transfer transaction
 * @transaction
 */
function cancelTransfer(cancelTransferRequest) {
    var transferUUId = cancelTransferRequest.detailtransferUUId.trim()
    if (!transferUUId) {
        throw new Error('Please provide an id of transaction to cancel');
    }
    var namespace = 'org.bcnet.payme';
    return getAssetRegistry(namespace + '.Transfer')
        .then(function (transferRegistry) {
            return transferRegistry.exists(transferUUId)
                .then(function (exists) {
                    if (exists) {
                        // cancel transfer
                        return transferRegistry.get(transferUUId)
                            .then(function (transfer) {
                                if (transfer.state == "Closed") {
                                    throw new Error("Transfer is already closed");
                                } else if (transfer.state == "Canceled") {
                                    throw new Error("Transfer is already canceled");
                                } else {
                                    transfer.state = "Canceled";
                                    transfer.canceledTS = getCurrentTimeStamp();
                                    return transferRegistry.update(transfer)
                                        .then(function () {
                                            // TODO: Implement transaction back
                                            return getParticipantRegistry(namespace + ".Account")
                                                .then(function (accountRegitry) {
                                                    return accountRegitry.get(transfer.payerEmail)
                                                        .then(function (payerAccount) {
                                                            payerAccount.balance += transfer.amount;
                                                            return accountRegitry.update(payerAccount)                                                                
                                                        });
                                                });

                                        });
                                }
                            })
                    } else {
                        throw new Error('Transfer does not exists');
                    }
                })
        });
}
/**
 * Conferm / close transfer
 * @param {org.bcnet.payme.confirmTransfer} confirmTransfer - the corfirm (close) transfer transaction
 * @transaction
 */
function confirmTransfer(confirmTransferRequest) {
    var namespace = 'org.bcnet.payme';
    var code = confirmTransferRequest.confirmationCode;
    var email = confirmTransferRequest.moneyReceiverAccountEmail;
    if (!code) {
        throw new Error('Please provide confirmation code');
    }
    if (!email) {
        throw new Error('Please provide account email of money receiver');
    }
    return getAssetRegistry(namespace + '.Transfer')
        .then(function (assetRegistry) {
            return query('selectTransferByConfirmationCodeAndToEmail',
                {
                    "confirmationCode": code,
                    "toAccountEmail": email
                })
                .then(function (results) {
                    if (results.length == 0) {
                        throw Error('No transfer found');
                    } else if (results.length > 1) {
                        throw Error('Multiple transfers found');
                    } else {
                        // process transfer
                        var transfer = results[0];
                        if (transfer.state == "Canceled") {
                            throw new Error('This transfer is already canceled');
                        }
                        if (transfer.state == "Closed") {
                            throw new Error('This transfer is already closed');
                        }
                        transfer.closedTS = getCurrentTimeStamp();
                        transfer.state = "Closed"
                        return assetRegistry.update(transfer)
                        .then(function(){
                            return getParticipantRegistry(namespace + '.Account')
                                .then(function (accountRegistry) {
                                    // update to account
                                    return accountRegistry.get(transfer.toAccountEmail)
                                        .then(function (toAccount) {
                                            toAccount.balance += transfer.amount;
                                            return accountRegistry.update(toAccount);
                                        });
                                });
                            });
                    }
                });
        });
}

/**
 * Sign up 
 * @param {org.bcnet.payme.signUp} signUp - the sign up transaction
 * @transaction
 */
function signUp(signUpRequest) {
    if (!validateEmail(signUpRequest.email.trim())) {
        throw new Error('Sign up email does not look like an email address');
    }
    if (!signUpRequest.password) {
        throw new Error('Please provide a password in order to sign up');
    }
    if (!signUpRequest.firstName.trim()) {
        throw new Error('Please provide a first name in order to sign up');
    }
    if (!signUpRequest.lastName.trim()) {
        throw new Error('Please provide a last name in order to sign up');
    }
    if (!signUpRequest.currency) {
        throw new Error('Please provide currency information');
    }
    var namespace = 'org.bcnet.payme';

    // get account registry
    return getParticipantRegistry(namespace + '.Account')
        .then(function (accountRegistry) {
            return accountRegistry.exists(signUpRequest.email)
                .then(function (exists) {
                    if (exists) {
                        throw new Error('This email is already registered');
                    } else {
                        // create new account and add it
                        var account = getFactory().newResource(namespace, 'Account', signUpRequest.email.trim());
                        account.passwordHash = hashFunction(signUpRequest.password.trim());
                        account.firstName = signUpRequest.firstName.trim();
                        account.lastName = signUpRequest.lastName.trim();
                        account.balance = 0;
                        account.currency = signUpRequest.currency;
                        return accountRegistry.add(account);
                    }
                })
        });
}

/**
 * Login
 * @param {org.bcnet.payme.login} login - the login transaction
 * @transaction
 */
function login(loginRequest) {
    if (!loginRequest.email.trim()) {
        throw new Error('Please provide login email');
    }
    if (!loginRequest.password.trim()) {
        throw new Error('Please provide password');
    }
    var namespace = 'org.bcnet.payme';
    return getParticipantRegistry(namespace + '.Account')
        .then(function (accountRegistry) {
            return accountRegistry.exists(loginRequest.email)
                .then(function (exists) {
                    if (exists) {
                        return accountRegistry.get(loginRequest.email)
                        .then(function (account){
                            if (account.passwordHash == hashFunction(loginRequest.password)) {
                                // user logged in, get back with 200
                                return;
                            } else {
                                throw new Error('Wrong password');
                            }
                        });
                    } else {
                        throw new Error('This account does not exist');
                    }
                })
        });
}

/**
 * Add money 
 * @param {org.bcnet.payme.addMoney} addMoney - the add money transaction
 * @transaction
 */
function addMoney(addMoneyRequest) {
    if (!addMoneyRequest.email.trim()) {
        throw new Error('Please provide email to add money to');
    }
    if (!addMoneyRequest.amount || addMoneyRequest.amount <= 0) {
        throw new Error('Please provide a valid amount to add');
    }
    var namespace = 'org.bcnet.payme';

    // get account registry
    return getParticipantRegistry(namespace + '.Account')
        .then(function (accountRegistry) {
            return accountRegistry.exists(addMoneyRequest.email)
                .then(function (exists) {
                    if (!exists) {
                        throw new Error('Account does not exist');
                    } else {
                        // add money
                        return accountRegistry.get(addMoneyRequest.email)
                        .then(function (account){
                            account.balance += addMoneyRequest.amount;
                            return accountRegistry.update(account);
                        });
                    }
                })
        });
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

function getCurrentTimeStamp() {
    var now = new Date();
    return now.toISOString();
}

function hashFunction(str) {
    /**
     * @todo add hash function
     */
    return SHA256(str);
}

/**
*
*  Secure Hash Algorithm (SHA256)
*  http://www.webtoolkit.info/
*  http://www.webtoolkit.info/javascript-sha256.html
*
*  Original code by Angel Marin, Paul Johnston.
*
**/
function SHA256(s){
    var chrsz   = 8;
    var hexcase = 0;
    function safe_add (x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }

    function R (X, n) { return ( X >>> n ); }

    function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }

    function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }

    function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }

    function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }

    function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }

    function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }

    function core_sha256 (m, l) {
        var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
        var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
        var W = new Array(64);
        var a, b, c, d, e, f, g, h, i, j;
        var T1, T2;
        m[l >> 5] |= 0x80 << (24 - l % 32);
        m[((l + 64 >> 9) << 4) + 15] = l;
        for ( var i = 0; i<m.length; i+=16 ) {
            a = HASH[0];
            b = HASH[1];
            c = HASH[2];
            d = HASH[3];
            e = HASH[4];
            f = HASH[5];
            g = HASH[6];
            h = HASH[7];
            for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));
                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
            }
            HASH[0] = safe_add(a, HASH[0]);
            HASH[1] = safe_add(b, HASH[1]);
            HASH[2] = safe_add(c, HASH[2]);
            HASH[3] = safe_add(d, HASH[3]);
            HASH[4] = safe_add(e, HASH[4]);
            HASH[5] = safe_add(f, HASH[5]);
            HASH[6] = safe_add(g, HASH[6]);
            HASH[7] = safe_add(h, HASH[7]);
        }
        return HASH;
    }

    function str2binb (str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for(var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
        }
        return bin;
    }

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }

    function binb2hex (binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for(var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
        }
        return str;
    }
    s = Utf8Encode(s);
    return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
}