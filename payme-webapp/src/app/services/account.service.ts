import Account from '../models/account.model';
import { Observable } from 'rxjs';
// import { Observable } from 'rxjs/add/operator/map'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';

//RxJS operator for mapping the observable
// import 'rxjs/add/operator/map';

@Injectable()
export class AccountService {

    api_url = 'http://localhost:3000';
    accountUrl = `${this.api_url}/api/accounts`;

    constructor(
        private http: HttpClient
    ) { }

    //Sign up account, takes a Account Object
    signUp(account: Account): Observable<any> {
        console.log('Sign up was called in account.service: email: ' + account._email +
            ', first name: ' + account.firstName +
            ', last name: ' + account.lastName +
            ', created ts: ' + account.createdTS);
        alert('Hey ' + account.firstName + '! This function is not implemented yet');
        // TODO: implement sign up logic here
        //returns the observable of http post request 
        return this.http.post(`${this.accountUrl}`, account);
    }

    //Update account, takes an Account Object as parameter
    editAccount(account: Account) {
        let editUrl = `${this.accountUrl}`
        //returns the observable of http put request 
        return this.http.put(editUrl, account);
    }

    //Default Error handling method.
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}