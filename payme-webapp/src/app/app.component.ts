import { Response } from '@angular/http'
import { AccountService } from './services/account.service'
import Account from './models/account.model'
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    // Private accountservice will be injected into the component by Angular Dependency Injector
    private accountService: AccountService
  ) { }

  //Declaring the new account Object and initilizing it
  public newAccount: Account = new Account()

  ngOnInit(): void {
    // here on init logic
  }

  //This method will get called on sign up button event

  signUp() {
    this.accountService.signUp(this.newAccount)
      .subscribe((res) => {
        this.newAccount = new Account()
      })
  }
}