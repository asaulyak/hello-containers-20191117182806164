class Account {
    _email: string;
    firstName: string;
    lastName: string;
    createdTS: Date;

    constructor(
    ) {
        this.firstName = ""
        this.lastName = ""
        this.createdTS = new Date()
    }
}

export default Account;