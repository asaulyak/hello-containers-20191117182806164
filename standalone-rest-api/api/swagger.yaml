openapi: 3.0.0
# Added by API Auto Mocking Plugin
servers:
  - description: Google Cloud
    url: http://35.233.120.35:3000
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/Paynow/Paynow/0.0.1
info:
  description: This is a paynow REST API
  version: "1.0.1"
  title: Paynow API
  contact:
    email: developer.payme@gmail.com
  license:
    name: Proprietary software license
    url: ""
tags:
  - name: developers
    description: Operations available to regular developers
paths:
  /account/signup:
    post:
      tags:
        - developers
      summary: adds a new account
      operationId: accountSignUp
      description: Adds an account to the system
      responses:
        '201':
          description: Account created
        '409':
          description: Account already exists
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AccountSignup'
        description: Account to add
  /account/login:
    post:
      tags:
        - developers
      summary: logins existing account
      operationId: accountLogin
      description: Checks if account email and password matches
      responses:
        '200':
          description: Auth successful
        '401':
          description: Auth failed
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Account'
        description: Account to login
  /account/get:
    post:
      tags:
        - developers
      summary: gets an existing account
      operationId: getAccount
      description: Gets account by email
      responses:
        '200':
          description: account
        '404':
          description: not found
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
  /account/get/all:
    get:
      tags:
        - developers
      summary: gets all existing account
      operationId: getAllAccounts
      description: Gets all accounts
      responses:
        '200':
          description: accounts
        '500':
          description: Technical error
  /account/change/password:
    post:
      tags:
        - developers
      summary: changes password of an existing account
      operationId: changePassword
      description: Changes the password of an existing account. Possible only if logged in.
      responses:
        '200':
          description: Password successfully changed
        '404':
          description: Account not found
        '401':
          description: Wrong credentials
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ChangePasswordLoggedin'
        description: Account to login
  /account/forgot/password:
    post:
      tags:
        - developers
      summary: sends a forgot password email
      operationId: forgotPassword
      description: sends a forgot password email if account exists
      responses:
        '200':
          description: Email to reset password is sent
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Email to reset password
  /check/password/{forgotPasswordToken}:
    get:
      tags:
        - developers
      summary: checks if password token is ok
      parameters:
        - in: path
          name: forgotPasswordToken
          schema:
            type: string
          required: true
          description: Forgot password token to account
      operationId: checkPasswordToken
      description: checks if password token is valid (not expiered, and does exist)
      responses:
        '200':
          description: Token is ok
        '401':
          description: Token was already used
        '404':
          description: Password token was not found
        '410':
          description: Token is expired
        '412':
          description: Token is invalid
        '500':
          description: Technical error
  /reset/password:
    post:
      tags:
        - developers
      summary: resets password by token
      operationId: resetPassword
      description: Resets password by forgot password token sent to email for an existing account with valid password token
      responses:
        '200':
          description: Token is ok
        '401':
          description: Token was already used
        '404':
          description: Password token was not found
        '410':
          description: Token is expired
        '412':
          description: Token is invalid
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ForgotPassword'
        description: Forgot password token
  /account/{verificationToken}:
    get:
      tags:
        - developers
      summary: verifies account
      parameters:
        - in: path
          name: verificationToken
          schema:
            type: string
          required: true
          description: Verification token to account
      operationId: verifyAccount
      description: verifies account if verification token is valid (not expiered, and does exist)
      responses:
        '201':
          description: Account verified
        '401':
          description: Token was already used
        '404':
          description: Not found
        '410':
          description: Token is expired
        '412':
          description: Account is already verified
        '500':
          description: Technical error
  /add/money:
    post:
      tags:
        - developers
      summary: adds money to account
      operationId: addMoney
      description: Adds money to an account
      responses:
        '200':
          description: Success
        '404':
          description: Account not found
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AccountBalance'
        description: Payer information
  /pay:
    post:
      tags:
        - developers
      summary: money transfer
      operationId: transfer
      description: Avaliable only for a logged in user with header 'Authorization' = 'Bearer ' + authToken
      responses:
        '200':
          description: Success
        '401':
          description: Auth error
        '404':
          description: Payer account not found
        '405':
          description: Account pay to not found
        '406':
          description: Account pay for not found
        '409':
          description: Payer does not have enough funds
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Transfer'
        description: Payer information
  /transfer/confirm:
    post:
      tags:
        - developers
      summary: confirms a transfer
      operationId: confirmTransfer
      description: Confirms a transfer
      responses:
        '200':
          description: Success
        '404':
          description: Not found
        '409':
          description: Transfer is already closed
        '410':
          description: Transfer is canceled
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ConfirmTransfer'
        description: Confirmation information
  /transfer/cancel:
    post:
      tags:
        - developers
      summary: cancels a transfer
      operationId: cancelTransfer
      description: Cancels a transfer if it is in open state
      responses:
        '200':
          description: Success
        '404':
          description: Not found
        '409':
          description: Transfer is already closed
        '410':
          description: Transfer is canceled
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CancelTransfer'
        description: Confirmation information
  /transfers/incoming/open:
    post:
      tags:
        - developers
      summary: Transfers query
      operationId: getOpenIncomingTransfersByEmail
      description: Gets open incoming Transfers by email
      responses:
        '200':
          description: Transfers
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
  /transfers/incoming/closed:
    post:
      tags:
        - developers
      summary: Transfers query
      operationId: getClosedIncomingTransfersByEmail
      description: Gets closed incoming Transfers by email
      responses:
        '200':
          description: Transfers
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
  /transfers/incoming:
    post:
      tags:
        - developers
      summary: Transfers query
      operationId: getOpenAndClosedIncomingTransfersByEmail
      description: Gets open and closed incoming Transfers by email
      responses:
        '200':
          description: Transfers
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
  /transfers/outgoing/open:
    post:
      tags:
        - developers
      summary: Transfers query
      operationId: getOpenOutgoingTransfersByEmail
      description: Gets open outgoing Transfers by email
      responses:
        '200':
          description: Transfers
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
  /transfers/outgoing/closed:
    post:
      tags:
        - developers
      summary: Transfers query
      operationId: getClosedOutgoingTransfersByEmail
      description: Gets closed outgoing Transfers by email
      responses:
        '200':
          description: Transfers
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
  /transfers/outgoing:
    post:
      tags:
        - developers
      summary: Transfers query
      operationId: getOpenAndClosedOutgoingTransfersByEmail
      description: Gets open and closed outgoing Transfers by email
      responses:
        '200':
          description: Transfers
        '500':
          description: Technical error
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
        description: Account email
components:
  schemas:
    Email:
      type: object
      required:
        - email
      properties:
        email:
          type: string
          example: test@test.com
    Account:
      type: object
      required:
        - email
        - password
      properties:
        email:
          type: string
          example: test@test.com
        password:
          type: string
          example: qwerty;123
    AccountSignup:
      type: object
      required:
        - email
        - password
        - firstName
        - lastName
      properties:
        email:
          type: string
          example: test@test.com
        password:
          type: string
          example: qwerty;123
        firstName:
          type: string
          example: Max
        lastName:
          type: String
          example: Musterman
    ForgotPassword:
      type: object
      required:
        - token
        - newPassword
      properties:
        token:
          type: string
          example: 7WniavYPbUBpdAQQk88TRu9dJHo6V8wjXVUNVclUL4c
        newPassword:
          type: string
          example: newpassword
    ChangePasswordLoggedin:
      type: object
      required:
        - email
        - oldPassword
        - newPassword
      properties:
        email: 
          type: string
          example: test@test.com
        oldPassword:
          type: string
          example: qwerty;123
        newPassword:
          type: string
          example: asDfg;7131
    Transfer:
      type: object
      required:
        - payerEmail
        - amount
        - forAccountEmail
        - toAccountEmail
      properties:
        payerEmail: 
          type: string
          example: test1@test.com
        amount: 
          type: number
          format: format
          example: 99.99
        forAccountEmail: 
          type: string
          example: test2@test.com
        toAccountEmail: 
          type: string
          example: test3@test.com
    AccountBalance:
      type: object
      required:
        - email
        - amount
      properties:
        email: 
          type: string
          example: test@test.com
        amount: 
          type: number
          format: format
          example: 99.99
    ConfirmTransfer:
      type: object
      required:
        - moneyReceiverAccountEmail
        - confirmationCode
      properties:
        moneyReceiverAccountEmail:
          type: string
          example: test@test.com
        confirmationCode:
          type: string
          example: dsfE34Fe
    CancelTransfer:
      type: object
      required:
        - transferUUID
        - payerEmail
      properties:
        transferUUID:
          type: string
          example: sjghuifdjskfuiHfesfu34dkfds3894d
        payerEmail:
          type: string
          example: test@test.com