var fs = require("fs");
const config = require("./config.json");
// const environment = process.env.NODE_ENV || "development";
const environment = process.env.NODE_ENV || "testing";
const environmentConfig = config[environment];
console.log(environmentConfig);
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const accountRoutes = require("./api/routes/account");
const verificationRoutes = require("./api/routes/verification");
const testingHelperRoutes = require("./api/routes/testingHelper");
const forgotPasswordRoutes = require("./api/routes/forgotPassword");
const transferRoutes = require("./api/routes/transfer");
const appConfigRoutes = require("./api/routes/appConfig");

var dbConnectionUrl = `mongodb://${environmentConfig.dbAddress}:${
  environmentConfig.dbPort
}/${environmentConfig.dbName}`;
if (environmentConfig.dbParams) {
  dbConnectionUrl += environmentConfig.dbParams;
}
var dbConnectionOptions;
if (environmentConfig.sslCAFileName) {
  console.log("CA file name: " + environmentConfig.sslCAFileName);
  var sslCA = [fs.readFileSync(environmentConfig.sslCAFileName)];
  dbConnectionOptions = {
    ssl: true,
    sslValidate: true,
    sslCA
  };
}
console.log("dbConnectionUrl = " + dbConnectionUrl);
console.log("dbConnectionOptions = " + dbConnectionOptions);
if (dbConnectionOptions) {
  mongoose.connect(
    dbConnectionUrl,
    dbConnectionOptions
  );
} else {
  mongoose.connect(dbConnectionUrl);
}

mongoose.Promise = global.Promise;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

app.use("/account", accountRoutes);
app.use("/verify", verificationRoutes);
app.use("/", forgotPasswordRoutes);
app.use("/", transferRoutes);
app.use("/", appConfigRoutes);
if (
  environmentConfig.config_id == "development" ||
  environmentConfig.config_id == "testing"
) {
  app.use("/test", testingHelperRoutes);
}

app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
