const http = require("http");
const app = require("./app");
const package = require("./package.json");

const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port);
console.log(
  new Date().toISOString() +
    " " +
    package.description +
    " v " +
    package.version +
    " server started on port: " +
    port
);
