const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const VerificationToken = require('../models/verificationToken');
const Account = require('../models/account');

exports.verify = (req, res, next) => {
    console.log("Verifiing " + req.params.token);
    VerificationToken.find({ token: req.params.token })
        .exec()
        .then(tokens => {
            if (tokens.length < 1) {
                return res.status(404).json({
                    message: 'Not found'
                });
            } else {
                // check if this token is not used already
                if (tokens[0].used) {
                    return res.status(401).json({
                        message: 'Token was already used'
                    });
                } else {
                    Account.find({ email: tokens[0].email })
                        .exec()
                        .then(accounts => {
                            if (accounts.length < 1) {
                                return res.status(404).json({
                                    message: 'Not found'
                                });
                            } else {
                                if (accounts[0].verified) {
                                    return res.status(409).json({
                                        message: 'Account is already verified'
                                    });
                                } else {
                                    accounts[0].verified = true;
                                    accounts[0]
                                        .save()
                                        .then(resultAccount => {
                                            res.status(201).json({
                                                message: 'Account verified'
                                            });
                                        })
                                        .catch(err => {
                                            console.log(err);
                                            res.status(500).json({
                                                error: err
                                            });
                                        });
                                }
                            }
                        })
                        .catch(err => {
                            console.log(err);
                            return res.status(500).json({
                                error: err
                            });
                        });
                }

            }
        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                error: err
            });
        });
}

// USED FOR TESTING ONLY! NOT AVALIABLE IN PRODUCTION!
exports.getVerificationEmailString = (req, res, next) => {
    console.log("Looking for verification token for email: " + req.body.email);
    VerificationToken.find({
        email: req.body.email
    })
        .where('used').equals(false)
        .exec()
        .then(verifications => {
            if (verifications.length < 1) {
                return res.status(404).json({
                    message: 'Not found'
                });
            } else {
                return res.status(200).json({
                    token: verifications[0].token
                });
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                error: err
            });
        });
}