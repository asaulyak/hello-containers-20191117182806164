const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const TokenGenerator = require('uuid-token-generator');
const config = require('../../config.json');
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
const timeOffset = environmentConfig.forgottenPasswordValidityTimeInSec;
const Account = require('../models/account');
const PasswordToken = require('../models/passwordToken');

exports.forgotPassword = (req, res, next) => {
    Account.find({ email: req.body.email })
        .exec()
        .then(accounts => {
            if (accounts.length < 1) {
                return res.status(201).json({
                    message: 'Email to reset password is sent'
                });
            } else {
                // invalid old tokens if there
                PasswordToken.find({ email: accounts[0].email })
                    .exec()
                    .then(tokens => {
                        for (var i = 0; i < tokens.length; i++) {
                            tokens[i].valid = false;
                            tokens[i].save();
                        }
                    })
                    .catch(err => {
                        console.log('Invalidating old password tokens failed with error: ' + err);
                    });
                // create new forgot password token
                const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
                var timeObject = new Date();
                // TODO: read config
                const expiryDate = new Date(timeObject.getTime() + timeOffset * 1000);
                const newToken = PasswordToken({
                    _id: new mongoose.Types.ObjectId(),
                    email: accounts[0].email,
                    creationTS: new Date().toISOString(),
                    token: tokgen2.generate(),
                    expiryDate: expiryDate.toISOString()
                });
                newToken
                    .save()
                    .then(resultToken => {
                        // TODO: add send email!
                        console.log('Sending forgot password email with token: ' + newToken.token);
                        res.status(201).json({
                            message: 'Email to reset password is sent'
                        });
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                error: err
            });
        });
}

// USED FOR TESTING ONLY! NOT AVALIABLE IN PRODUCTION!
exports.getLastPasswordTokenByEmail = (req, res, next) => {
    PasswordToken.find({
        email: req.body.email
    })
        .sort({ creationTS: -1 })
        .exec()
        .then(tokens => {
            if (tokens.length < 1) {
                return res.status(404).json({
                    message: 'Password token was not found'
                });
            } else {
                // create new forgot password token 
                return res.status(200).json({
                    token: tokens[0].token
                });
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                error: err
            });
        });
}

/*
* Checks if forgot password token is ok and not exipred
*/
exports.checkForgotPasswordToken = (req, res, next) => {
    PasswordToken.find({ token: req.params.token })
        .exec()
        .then(tokens => {
            if (tokens.length < 1) {
                return res.status(404).json({
                    message: 'Password token was not found'
                });
            } else if (tokens[0].valid == false) {
                return res.status(412).json({
                    message: 'Token is invalid'
                });
            } else if (isTokenExpired(tokens[0])) {
                return res.status(410).json({
                    message: 'Token is expired'
                });
            } else {
                // check if token not expieren
                if (tokens[0].used) {
                    return res.status(401).json({
                        message: 'Token was already used'
                    });
                } else {
                    return res.status(200).json({
                        message: 'Token is ok'
                    });
                }
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                error: err
            });
        });
}

/*
* Checks if forgot password token is ok and not exipred and resets password
*/
exports.resetPassword = (req, res, next) => {
    PasswordToken.find({
        token: req.body.token
    })
        .exec()
        .then(tokens => {
            if (tokens.length < 1) {
                return res.status(404).json({
                    message: 'Password token was not found'
                });
            } else {
                console.log('Found at ' + new Date().toISOString() + ' token: ' + tokens[0]);
                console.log('expiryDate > now ?: ' + isTokenExpired(tokens[0]));
                if (tokens[0].used) {
                    return res.status(401).json({
                        message: 'Token was already used'
                    });
                }
                else if (isTokenExpired(tokens[0])) {
                    return res.status(410).json({
                        message: 'Token is expired'
                    });
                } else if (tokens[0].valid == false) {
                    return res.status(412).json({
                        message: 'Token is invalid'
                    });
                }
                else {
                    // TODO: check if the token is not expired
                    console.log('resetPassword: email = ' + tokens[0].email);
                    Account.find({ email: tokens[0].email })
                        .exec()
                        .then(accounts => {
                            if (accounts.length < 1) {
                                return res.status(404).json({
                                    message: 'Password token was not found'
                                });
                            } else {
                                tokens[0].used = true;
                                tokens[0]
                                    .save()
                                    .then(resultToken => {
                                        bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
                                            if (err) {
                                                return res.status(500).json({
                                                    error: err
                                                });
                                            } else {
                                                accounts[0].password = hash;
                                                accounts[0]
                                                    .save()
                                                    .then(result => {
                                                        res.status(201).json({
                                                            message: 'New password is set'
                                                        });
                                                    })
                                                    .catch(err => {
                                                        console.log(err);
                                                        res.status(500).json({
                                                            error: err
                                                        });
                                                    });
                                            }
                                        });
                                    })
                                    .catch(err => {
                                        console.log(err);
                                        res.status(500).json({
                                            error: err
                                        });
                                    });

                            }
                        })
                        .catch(err => {
                            console.log(err);
                            return res.status(500).json({
                                error: err
                            });
                        });
                }
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                error: err
            });
        });
}

function isTokenExpired(token) {
    return token.expiryDate.valueOf() < new Date().valueOf();
}