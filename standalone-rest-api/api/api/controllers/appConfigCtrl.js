const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const AppConfig = require("../models/appConfig");

exports.appConfigSet = (req, res, next) => {
    AppConfig.find({
            key: req.body.key
        })
        .exec()
        .then(appConfigs => {
            if (appConfigs.length >= 1) {
                appConfigs[0].value = req.body.value;
                appConfigs[0]
                    .save()
                    .then(
                        resultAppConfigEntry => {
                            console.log(
                                "Status updated"
                            );
                            res.status(200).json({
                                message: "Success"
                            });
                        })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
            } else {
                const appConfigEntry = AppConfig({
                    key: req.body.key,
                    value: req.body.value
                });
                appConfigEntry
                    .save()
                    .then(resultAppConfigEntry => {
                        console.log(
                            "Status created"
                        );
                        // TODO: add send email!
                        res.status(200).json({
                            message: "Success"
                        });
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.statusCheck = (req, res, next) => {
    AppConfig.find({
            key: "status"
        })
        .exec()
        .then(appConfigs => {
            if (appConfigs.length >= 1) {
                if (appConfigs[0].value == "OK") {
                    console.log("Checking status: 200");
                    res.status(200).json({
                        message: appConfigs[0].value
                    });
                } else {
                    console.log("Checking status: 503");
                    res.status(503).json({
                        message: "something else"
                    });
                }
            } else {
                console.log("Checking status: 404");
                res.status(404).json({
                    message: "Not found"
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}