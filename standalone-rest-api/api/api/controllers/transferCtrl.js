const mongoose = require("mongoose");

const Account = require("../models/account");
const Transfer = require("../models/transfer");
const TokenGenerator = require("uuid-token-generator");

exports.pay = (req, res, next) => {
  var payer;
  var newTransfer;
  Account.find({ email: req.body.payerEmail })
    .exec()
    .then(accounts => {
      if (accounts.length < 1) {
        return res.status(404).json({
          message: "Payer account not found"
        });
      } else {
        payer = accounts[0];
        if (payer.balance < req.body.amount) {
          return res.status(409).json({
            message: "Payer does not have enough funds"
          });
        } else {
          Account.find({ email: req.body.toAccountEmail })
            .exec()
            .then(account => {
              if (account.length < 1) {
                return res.status(405).json({
                  message: "Account pay to not found"
                });
              } else {
                Account.find({ email: req.body.forAccountEmail })
                  .exec()
                  .then(account => {
                    if (account.length < 1) {
                      return res.status(404).json({
                        message: "Account pay for not found"
                      });
                    } else {
                      // substract money from payer
                      payer.balance -= req.body.amount;
                      const tokgen256 = new TokenGenerator(
                        256,
                        TokenGenerator.BASE62
                      );
                      // Create new Transfer
                      const transfer = Transfer({
                        _id: new mongoose.Types.ObjectId(),
                        uuid: tokgen256.generate(),
                        amount: req.body.amount,
                        payerEmail: req.body.payerEmail,
                        forAccountEmail: req.body.forAccountEmail,
                        toAccountEmail: req.body.toAccountEmail,
                        transferState: "open",
                        confirmationCode: tokgen256.generate().substring(0, 6)
                      });
                      transfer
                        .save()
                        .then(result => {
                          console.log("New transfer created: " + result);
                          newTransfer = result;
                          // save payer account
                          payer
                            .save()
                            .then(result => {
                              res.status(200).json({
                                message: "Success",
                                transferID: newTransfer.uuid,
                                confirmationCode: newTransfer.confirmationCode
                              });
                            })
                            .catch(err => {
                              console.log(err);
                              res.status(500).json({
                                error: err
                              });
                            });
                        })
                        .catch(err => {
                          console.log(err);
                          res.status(500).json({
                            error: err
                          });
                        });
                    }
                  })
                  .catch(err => {
                    console.log(err);
                    return res.status(500).json({
                      error: err
                    });
                  });
              }
            })
            .catch(err => {
              console.log(err);
              return res.status(500).json({
                error: err
              });
            });
        }
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.confirmTransfer = (req, res, next) => {
  var transferToConfirm;
  Transfer.find({
    toAccountEmail: req.body.moneyReceiverAccountEmail,
    confirmationCode: req.body.confirmationCode
  })
    .exec()
    .then(transfers => {
      console.log(
        "Trying to confirm transfer with code: " + req.body.confirmationCode
      );
      if (transfers.length < 1) {
        return res.status(404).json({
          message: "Not found"
        });
      } else {
        transferToConfirm = transfers[0];
        console.log("Transfer to confirm with uuid: " + transferToConfirm.uuid);
        if (transferToConfirm.closedTS) {
          return res.status(409).json({
            message: "Transfer is already closed"
          });
        } else if (transferToConfirm.canceledTS) {
          return res.status(410).json({
            message: "Transfer is canceled"
          });
        } else {
          Account.find({
            email: transferToConfirm.toAccountEmail
          })
            .exec()
            .then(accounts => {
              if (accounts.length < 1) {
                return res.status(404).json({
                  message: "Not found"
                });
              } else {
                accounts[0].balance += transferToConfirm.amount;
                accounts[0]
                  .save()
                  .then(result => {
                    transferToConfirm.transferState = "closed";
                    transferToConfirm.closedTS = new Date().toISOString();
                    transferToConfirm
                      .save()
                      .then(result => {
                        return res.status(200).json({
                          message: "Success"
                        });
                      })
                      .catch(err => {
                        console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });
                  })
                  .catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
              }
            })
            .catch(err => {
              console.log(err);
              return res.status(500).json({
                error: err
              });
            });
        }
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.cancelTransfer = (req, res, next) => {
  // o String transferUUID

  var transferToCancel;
  Transfer.find({
    uuid: req.body.transferUUID,
    payerEmail: req.body.payerEmail
  })
    .exec()
    .then(transfers => {
      console.log(
        "Found: " +
          transfers.length +
          " transfers trying to cancel transfer with uuid: " +
          req.body.transferUUID
      );
      if (transfers.length < 1) {
        return res.status(404).json({
          message: "Not found"
        });
      } else {
        transferToCancel = transfers[0];
        if (transferToCancel.closedTS) {
          return res.status(409).json({
            message: "Transfer is already closed"
          });
        } else if (transferToCancel.canceledTS) {
          return res.status(410).json({
            message: "Transfer is canceled"
          });
        } else {
          Account.find({
            email: transferToCancel.payerEmail
          })
            .exec()
            .then(accounts => {
              if (accounts.length < 1) {
                return res.status(404).json({
                  message: "Not found"
                });
              } else {
                accounts[0].balance += transferToCancel.amount;
                accounts[0]
                  .save()
                  .then(result => {
                    transferToCancel.transferState = "canceled";
                    transferToCancel.canceledTS = new Date().toISOString();
                    transferToCancel
                      .save()
                      .then(result => {
                        return res.status(200).json({
                          message: "Success"
                        });
                      })
                      .catch(err => {
                        console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });
                  })
                  .catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
              }
            })
            .catch(err => {
              console.log(err);
              return res.status(500).json({
                error: err
              });
            });
        }
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.addMoney = (req, res, next) => {
  console.log("Adding money to account: " + req.body.email);
  Account.find({ email: req.body.email })
    .exec()
    .then(accounts => {
      console.log("addMoney: Found " + accounts.length + " accounts");
      if (accounts.length < 1) {
        return res.status(404).json({
          message: "Account not found"
        });
      } else {
        accounts[0].balance += req.body.amount;
        accounts[0]
          .save()
          .then(result => {
            return res.status(200).json({
              message: "Success"
            });
          })
          .catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.getOpenIncomingTransfersByEmail = (req, res, next) => {
  Transfer.find({ toAccountEmail: req.body.email, transferState: "open" })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getClosedIncomingTransfersByEmail = (req, res, next) => {
  Transfer.find({ toAccountEmail: req.body.email, transferState: "closed" })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getOpenAndClosedIncomingTransfersByEmail = (req, res, next) => {
  Transfer.find({
    $or: [
      { toAccountEmail: req.body.email, transferState: "open" },
      { toAccountEmail: req.body.email, transferState: "closed" }
    ]
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getOpenOutgoingTransfersByEmail = (req, res, next) => {
  Transfer.find({ payerEmail: req.body.email, transferState: "open" })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getClosedOutgoingTransfersByEmail = (req, res, next) => {
  Transfer.find({ payerEmail: req.body.email, transferState: "closed" })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getOpenAndClosedOutgoingTransfersByEmail = (req, res, next) => {
  Transfer.find({
    $or: [
      { payerEmail: req.body.email, transferState: "open" },
      { payerEmail: req.body.email, transferState: "closed" }
    ]
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getAllOpenAndClosedIncomingAndOutgoingTransfersByEmail = (
  req,
  res,
  next
) => {
  Transfer.find({
    $or: [
      { toAccountEmail: req.body.email, transferState: "open" },
      { toAccountEmail: req.body.email, transferState: "closed" },
      { payerEmail: req.body.email, transferState: "open" },
      { payerEmail: req.body.email, transferState: "closed" }
    ]
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getAllOpenIncomingAndOutgoingTransfersByEmail = (req, res, next) => {
  Transfer.find({
    $or: [
      { toAccountEmail: req.body.email, transferState: "open" },
      { payerEmail: req.body.email, transferState: "open" }
    ]
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getAllClosedIncomingAndOutgoingTransfersByEmail = (req, res, next) => {
  Transfer.find({
    $or: [
      { toAccountEmail: req.body.email, transferState: "closed" },
      { payerEmail: req.body.email, transferState: "closed" }
    ]
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getOpenAndClosedForTransfers = (req, res, next) => {
  Transfer.find({
    $or: [
      { forAccountEmail: req.body.email, transferState: "open" },
      { forAccountEmail: req.body.email, transferState: "closed" }
    ]
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getOpenForTransfers = (req, res, next) => {
  Transfer.find({
    forAccountEmail: req.body.email,
    transferState: "open"
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.getClosedForTransfers = (req, res, next) => {
  Transfer.find({
    forAccountEmail: req.body.email,
    transferState: "closed"
  })
    .exec()
    .then(transfers => {
      console.log(transfers);
      return res.status(200).json(transfers);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
