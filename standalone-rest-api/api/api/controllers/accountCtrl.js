const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Account = require("../models/account");
const VerificationToken = require("../models/verificationToken");

const TokenGenerator = require("uuid-token-generator");

exports.signup = (req, res, next) => {
  Account.find({ email: req.body.email })
    .exec()
    .then(account => {
      if (account.length >= 1) {
        return res.status(409).json({
          message: "Account exists"
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const account = Account({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash,
              firstName: req.body.firstName,
              lastName: req.body.lastName
            });
            account
              .save()
              .then(result => {
                console.log("New account created: " + result);
                // Sending verification email
                const tokgen2 = new TokenGenerator(256, TokenGenerator.BASE62);
                const verification = VerificationToken({
                  _id: new mongoose.Types.ObjectId(),
                  email: account.email,
                  token: tokgen2.generate()
                });
                verification
                  .save()
                  .then(resultVerification => {
                    console.log(
                      "Sending verification email with token: " +
                        verification.token
                    );
                    // TODO: add send email!
                    res.status(201).json({
                      message: "Account created"
                    });
                  })
                  .catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          }
        });
      }
    });
};

exports.login = (req, res, next) => {
  Account.find({ email: req.body.email })
    .exec()
    .then(accounts => {
      if (accounts.length < 1) {
        return res.status(401).json({
          message: "Auth failed"
        });
      }
      bcrypt.compare(req.body.password, accounts[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              email: accounts[0].email,
              accountId: accounts[0]._id
            },
            process.env.JWT_KEY,
            {
              expiresIn: "1h"
            }
          );
          return res.status(200).json({
            message: "Auth successful",
            token: token
          });
        }
        return res.status(401).json({
          message: "Auth failed"
        });
      });
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.getAccount = (req, res, next) => {
  console.log("getAccount: " + req.body.email);
  Account.find({ email: req.body.email })
    .exec()
    .then(account => {
      console.log("Found accounts " + account.length);
      if (account.length >= 1) {
        console.log(account[0]);
        return res.status(200).json(account[0]);
      } else {
        return res.status(404).json({
          message: "Account does not exist"
        });
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.getAllAccounts = (req, res, next) => {
  console.log("getAccount: " + req.body.email);
  Account.find()
    .exec()
    .then(accounts => {
      return res.status(200).json(accounts);
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};

exports.changePassword = (req, res, next) => {
  Account.find({ email: req.body.email })
    .exec()
    .then(accounts => {
      if (accounts.length < 1) {
        return res.status(404).json({
          message: "Account not found"
        });
      }
      bcrypt.compare(
        req.body.oldPassword,
        accounts[0].password,
        (err, result) => {
          if (err) {
            return res.status(401).json({
              message: "Wrong credentials"
            });
          }
          if (result) {
            console.assert("changePassword after bcrypt.compare");
            // Request has right credentials -> reset password
            bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
              if (err) {
                return res.status(500).json({
                  error: err
                });
              } else {
                accounts[0].password = hash;
                accounts[0]
                  .save()
                  .then(result => {
                    return res.status(200).json({
                      message: "Password successfully changed"
                    });
                  })
                  .catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
              }
            });
          } else {
            return res.status(401).json({
              message: "Wrong credentials"
            });
          }
        }
      );
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
};
