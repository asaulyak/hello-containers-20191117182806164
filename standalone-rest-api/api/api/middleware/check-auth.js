const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        console.log("Trying to decode "+token+" "+process.env.JWT_KEY);
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        console.log("Decoded");
        req.accountData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Auth failed'
        });
    }
};