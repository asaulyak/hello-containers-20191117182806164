const express = require('express');
const router = express.Router();

const AppConfigController = require('../controllers/appConfigCtrl');

router.post('/app/set', AppConfigController.appConfigSet);

router.get('/app/check/status', AppConfigController.statusCheck);

module.exports = router;