const express = require('express');
const router = express.Router();

const VerificationController = require('../controllers/verificationCtrl');

router.get('/account/:token', VerificationController.verify);

module.exports = router;