const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const AccountController = require('../controllers/accountCtrl');

router.post('/signup', AccountController.signup);

router.post('/login', AccountController.login);

router.post('/change/password', checkAuth, AccountController.changePassword);

router.post('/get', AccountController.getAccount);

router.get('/get/all', AccountController.getAllAccounts);

module.exports = router;