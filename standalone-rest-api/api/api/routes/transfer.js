const express = require("express");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");

const TransferController = require("../controllers/transferCtrl");

router.post("/pay", checkAuth, TransferController.pay);
router.post("/add/money", TransferController.addMoney);
router.post("/transfer/confirm", TransferController.confirmTransfer);
router.post("/transfer/cancel", TransferController.cancelTransfer);

// queries
router.post(
  "/transfers/incoming/open",
  TransferController.getOpenIncomingTransfersByEmail
);
router.post(
  "/transfers/incoming/closed",
  TransferController.getClosedIncomingTransfersByEmail
);
router.post(
  "/transfers/incoming",
  TransferController.getOpenAndClosedIncomingTransfersByEmail
);
router.post(
  "/transfers/outgoing/open",
  TransferController.getOpenOutgoingTransfersByEmail
);
router.post(
  "/transfers/outgoing/closed",
  TransferController.getClosedOutgoingTransfersByEmail
);
router.post(
  "/transfers/outgoing",
  TransferController.getOpenAndClosedOutgoingTransfersByEmail
);
router.post("/transfers/for", TransferController.getOpenAndClosedForTransfers);
router.post("/transfers/for/open", TransferController.getOpenForTransfers);
router.post("/transfers/for/closed", TransferController.getClosedForTransfers);
router.post(
  "/transfers/all",
  TransferController.getAllOpenAndClosedIncomingAndOutgoingTransfersByEmail
);
router.post(
  "/transfers/all/open",
  TransferController.getAllOpenIncomingAndOutgoingTransfersByEmail
);
router.post(
  "/transfers/all/closed",
  TransferController.getAllClosedIncomingAndOutgoingTransfersByEmail
);
module.exports = router;
