const express = require('express');
const router = express.Router();

const VerificationController = require('../controllers/verificationCtrl');
const ForgotPasswordToken = require('../controllers/forgotPasswordCtrl');
const checkAuth = require('../middleware/check-auth');

router.post('/get/verification/token', VerificationController.getVerificationEmailString);
router.post('/get/password/token', ForgotPasswordToken.getLastPasswordTokenByEmail)
router.post('/testAuth', checkAuth, (req, res, next) => {
    return res.status(200).json({
        message: 'Test successful'
    });
});
module.exports = router;