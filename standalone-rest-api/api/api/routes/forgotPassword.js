const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const ForgotPasswordController = require('../controllers/forgotPasswordCtrl');

router.post('/forgot/password', ForgotPasswordController.forgotPassword);

router.get('/check/password/:token', ForgotPasswordController.checkForgotPasswordToken);

router.post('/reset/password', ForgotPasswordController.resetPassword);

module.exports = router;