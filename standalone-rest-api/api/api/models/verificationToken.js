const mongoose = require('mongoose');

const verificationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    token: {
        type: String,
        required: true,
        unique: true
    },
    email: { 
        type: String, 
        required: true, 
        unique: false, 
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    creationTS: {
        type: Date,
        default: new Date().toISOString()
    },
    used: {
        type: Boolean,
        required: true,
        default: false
    }
    // expieryDate: {
    //     type: Date,
    //     required: true
    // }
});

module.exports = mongoose.model('VerificationToken', verificationSchema);