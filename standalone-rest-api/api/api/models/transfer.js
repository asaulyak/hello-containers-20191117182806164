const mongoose = require("mongoose");

const transferSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  uuid: {
    type: String,
    required: true,
    unique: true
  },
  amount: {
    type: Number,
    required: true
  },
  payerEmail: {
    type: String,
    required: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  forAccountEmail: {
    type: String,
    required: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  toAccountEmail: {
    type: String,
    required: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  transferState: {
    type: String,
    required: true,
    default: "open"
  },
  confirmationCode: {
    type: String,
    required: true
  },
  creationTS: {
    type: Date,
    required: true,
    default: new Date().toISOString()
  },
  closedTS: {
    type: Date
  },
  canceledTS: {
    type: Date
  }
});

module.exports = mongoose.model("Transfer", transferSchema);
