const mongoose = require('mongoose');

const appConfigSchema = mongoose.Schema({
    key: { 
        type: String, 
        required: true,
        unique: true
    },
    value: {
        type: String
    }
});

module.exports = mongoose.model('AppConfig', appConfigSchema);