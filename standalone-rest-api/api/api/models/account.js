const mongoose = require('mongoose');

const accountSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: { 
        type: String, 
        required: true, 
        unique: true, 
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: { type: String, required: true },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    balance: {
        type: Number,
        required: true,
        default: 0
    },
    verified: {
        type: Boolean,
        default: false
    },
    creationTS: {
        type: Date,
        required: true,
        default: new Date().toISOString()
    }
});

module.exports = mongoose.model('Account', accountSchema);